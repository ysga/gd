extends Node2D

onready var color := $ColorPicker
onready var avatar := $avatar
onready var dropdown = $ItemList
var active_part = 0
var parts = []

func add_color_presets():
	color.add_preset(Color((1.0/255)*104, (1.0/255)*64, (1.0/255)*45))
	color.add_preset(Color((1.0/255)*255, (1.0/255)*203, (1.0/255)*197))
	color.add_preset(Color((1.0/255)*255, (1.0/255)*215, (1.0/255)*0))
	color.add_preset(Color((1.0/255)*8, (1.0/255)*146, (1.0/255)*0))
	color.add_preset(Color((1.0/255)*255, (1.0/255)*255, (1.0/255)*255))
	color.add_preset(Color((1.0/255)*0, (1.0/255)*0, (1.0/255)*0))
	
	pass

func _ready():
	add_color_presets()
	parts = [
		avatar.get_node("hair"), 
		avatar.get_node("head"), 
		avatar.get_node("eyes"), 
		avatar.get_node("body")]

	dropdown.add_item("hair")
	dropdown.add_item("head")
	dropdown.add_item("eyes")
	dropdown.add_item("body")

func _process(_delta):
	active_part = dropdown.get_selected_id()
	parts[active_part].modulate = color.color
