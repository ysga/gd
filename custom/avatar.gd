extends KinematicBody2D

export (int) var speed = 100
var velocity: Vector2 = Vector2(0.0, 0.0)

func _ready():
	get_node("animation").play("S")
	
func _process(_delta):
	get_input()
	decide_animation()
	velocity = move_and_slide(velocity)

func get_input():
	velocity = Vector2()
	if Input.is_action_pressed("right"):
		velocity.x += 1
	if Input.is_action_pressed("left"):
		velocity.x -= 1
	if Input.is_action_pressed("down"):
		velocity.y += 1
	if Input.is_action_pressed("up"):
		velocity.y -= 1
	velocity = velocity.normalized() * speed

func decide_animation():
	if velocity.x == 0 and velocity.y > 0:
		get_node("animation").play("S")
	elif velocity.x > 0 and velocity.y > 0:
		get_node("animation").play("SE")
	elif velocity.x > 0 and velocity.y == 0:
		get_node("animation").play("E")
	elif velocity.x > 0 and velocity.y < 0:
		get_node("animation").play("NE")
	elif velocity.x == 0 and velocity.y < 0:
		get_node("animation").play("N")
	elif velocity.x < 0 and velocity.y < 0:
		get_node("animation").play("NW")
	elif velocity.x < 0 and velocity.y == 0:
		get_node("animation").play("W")
	elif velocity.x < 0 and velocity.y > 0:
		get_node("animation").play("SW")
	else:
		get_node("animation").stop()
